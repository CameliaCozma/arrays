package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int max;
        int N = 6;


        Scanner sc = new Scanner(System.in);

        System.out.println("Introdu dimensiunea sirului: ");
        N = sc.nextInt();
        int array[] = new int[N];
        for (int i = 0; i < N; i++) {
            System.out.println("Introdu un numar " + i);
            array[i] = sc.nextInt();

        }
        maxNumber(array);
        minNumber(array);
        evenNumber(array);
        oddNumber(array);
        reverseNumber(array);
    }

    public static void maxNumber(int array[]) {
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < array.length; i++) {
            if (max < array[i]) {
                max = array[i];

            }
        }
        System.out.println("Afiseza maximul din numar: " + max);
    }

    public static void minNumber(int array[]) {
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < array.length; i++) {
            if (min > array[i]) {
                min = array[i];

            }
        }
        System.out.println("Afiseaza minimul din numar: " + min);
    }

    public static void evenNumber(int array[]) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 0) {
                System.out.println("Afiseaza numerele pare: " + array[i]);

            }
        }
    }

    public static void oddNumber(int array[]) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 != 0) {
                System.out.println("Afiseaza numerele impare: " + array[i]);

            }
        }
    }
    public static void reverseNumber (int array[]){
        int arrayReverse[] = new int[array.length];

        for (int i = array.length-1; i>= 0; i--){
            arrayReverse[array.length-i-1] = array[i];
        }

        for (int i = 0; i < array.length; i++){
            System.out.println(" Afiseaza reversul " + arrayReverse[i]);
        }
    }

}
